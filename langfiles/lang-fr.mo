��    1      �  C   ,      8     9     A     J     S     b  
   j     u  
   z     �     �     �     �     �     �     �     �     �     �     �            	                   )     6     >     K     X     g     s     �  	   �     �     �     �     �     �  ,   �  #   �  	             "     '  &   4     [     _     c  �  h      	  	   )	  
   3	     >	     O	  
   W	     b	     j	     w	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     
     4
     E
     I
  	   R
     \
     k
     }
     �
     �
     �
     �
     �
     �
     �
     �
     �
       6     0   F     w     |     �     �      �     �     �     �     %         0                           &             '              	   "   (   -      #            !       +   
          .      )                 *          1                                            ,      /                 $        %s hour %s hours Category Completed task Contact Created by Deal Delegation Delete this task Description Detail Display type Done Due date Duplicate this task Edit Edit this task Email:  Is completed List Mark as done Next week No Not done Organization Phone:  Planned work Portal tools Remaining work Responsible Responsible user Status Sub-tasks Summary Task Task manager Taskmanager Tasks The task "<b>%s</b>" has been marked as done The task summary must not be empty. This week Today Type View details We have a problem finding this page... Yes day days Project-Id-Version: taskmanager
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-15 13:14+0100
PO-Revision-Date: 2019-02-15 13:14+0100
Last-Translator: Laurent Choulette <laurent.choulette@cantico.fr>
Language-Team: Cantico <laurent.choulette@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: taskmanager_translate;taskmanager_translate:1,2;translate;translate:1,2;translatable;translatable:1,2;taskmanager_translatable;taskmanager_translatable:1,2
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: programs
 %s heure %s heures Catégorie Tâche terminée Contact Créé par Affaire Délégation Supprimer cette tâche Description Détails Type d'affichage Terminé Date d'échéance Dupliquer cette tâche Modifier Modifier cette tâche Email :  Terminé Liste Marquer comme terminé Semaine suivante Non À faire Organisme Téléphone :  Travail planifié Outils portail Travail restant Responsable Responsable État Sous-tâches Résumé Tâche Gestionnaire de tâches Gestionnaire de tâches Tâches La tâche "<b>%s</b>" a été marquée comme terminée Le résumé de la tâche ne doit pas être vide. Week Aujourd'hui Type Détails Impossible de trouver la page... Oui jour jours 