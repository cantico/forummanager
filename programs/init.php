<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';



function forummanager_upgrade($version_base, $version_ini)
{
    $addonName = 'forummanager';
    $addon = bab_getAddonInfosInstance($addonName);

    $addonPhpPath = $addon->getPhpPath();

    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';

    require_once dirname(__FILE__) . '/forummanager.php';

    $functionalities = new bab_functionalities();

    if ($functionalities->registerClass('Func_App_Forummanager', $addonPhpPath . 'forummanager.php')) {
        echo(bab_toHtml('Functionality "Func_App_Forummanager" registered.'));
    }

    $App = forummanager_App();


    $mysqlBackend = new ORM_MySqlBackend(bab_getDB());

    $forumSet = $App->ForumSet();
    $sql = $mysqlBackend->setToSql($forumSet) . "\n";


    $synchronize = new bab_synchronizeSql();
    $synchronize->fromSqlString($sql);


    $addon->removeAllEventListeners();

    $addonPhpPath = $addon->getPhpPath();

    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'forummanager_onSiteMapItems', 'init.php');
    $addon->addEventListener('bab_eventBeforePageCreated', 'forummanager_onBeforePageCreated', 'init.php', -10);

    @bab_functionality::includefile('PortletBackend');

    if (class_exists('Func_PortletBackend')) {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        require_once dirname(__FILE__) . '/portletbackend.class.php';
        $functionalities = new bab_functionalities();
        $functionalities->registerClass('Func_PortletBackend_Forummanager', $addonPhpPath . 'portletbackend.class.php');
    }

    @bab_functionality::includefile('WorkspaceAddon');
    if (class_exists('Func_WorkspaceAddon')) {
        $addonPhpPath = $addon->getPhpPath();
        require_once dirname(__FILE__) . '/workspaceaddon.class.php';
        $functionalities->registerClass('Func_WorkspaceAddon_Forummanager', $addonPhpPath . 'workspaceaddon.class.php');
    }

    return true;
}



function forummanager_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
}



/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function forummanager_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';

    bab_functionality::includefile('Icons');

    $App = forummanager_App();


    $item = $event->createItem('forummanager_root');
    $item->setLabel($App->translate('Forum manager'));
    $item->setLink($App->Controller()->Forum()->displayList()->url());
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    $item->addIconClassname(Func_Icons::APPS_TASK_MANAGER);
    $event->addFolder($item);
}
