<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = forummanager_App();

$App->includeTraceableRecordSet();


/**
 * @param ORM_PkField                   $id
 * @param ORM_IntField                  $views
 * @param ORM_DatetimeField             $date
 * @param ORM_IntField                  $starter
 *
 * @param forummanager_ForumSet         $forum
 * @param forummanager_PostSet          $post
 * @param forummanager_PostSet          $lastpost
 *
 * @method forummanager_Thread get()
 * @method forummanager_Thread request()
 * @method forummanager_Thread newRecord()
 * @method forummanager_Thread[] select()
 *
 * @method Func_App_Forummanager    App()
 */
class forummanager_ThreadSet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName(BAB_THREADS_TBL);
        $App = $this->App();

        $this->setDescription($App->translatable('Thread'));

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('views'),
            ORM_DatetimeField('date')
                ->setDescription($App->translatable('Date')),
            ORM_UserField('starter')
                ->setDescription($App->translatable('Starter'))
        );

        $this->hasOne('forum', $App->ForumSetClassName());

        $this->hasOne('post', $App->PostClassName());

        $this->hasOne('lastpost', $App->PostClassName());
    }


    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @param int                       $id
 * @param int                       $views
 * @param string                    $date
 * @param ORM_IntField              $starter
 *
 * @param forummanager_Forum        $forum
 * @param forummanager_Post         $post
 * @param forummanager_Post         $lastpost
 */
class forummanager_Thread extends app_Record
{
    public function getRecordTitle()
    {

        return $this->post()->subject;
        return $this->oParentSet->post()->subject->output($this->post()->subject);
    }
}
