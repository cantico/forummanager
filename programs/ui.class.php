<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = forummanager_App();

$App->includeUi();

/**
 * The forummanager_Ui class
 */
class forummanager_Ui extends app_Ui
{
    /**
     * @param Func_App $app
     */
    public function __construct()
    {
        $this->setApp(forummanager_App());
    }

    /**
     * Includes Forum Ui helper functions definitions.
     */
    public function includeForum()
    {
        require_once FUNC_FORUMMANAGER_UI_PATH . 'forum.ui.php';
    }



    /**
     * Forum table view
     * @return forummanager_ForumTableView
     */
    public function ForumTableView()
    {
        $this->includeForum();
        return new forummanager_ForumTableView($this->App());
    }


    /**
     * Forum table view
     * @return forummanager_ForumCardsView
     */
    public function ForumCardsView()
    {
        $this->includeForum();
        return new forummanager_ForumCardsView($this->App());
    }


    /**
     * @param forummanager_Forum $forum
     * @return forummanager_ForumCardFrame
     */
    public function ForumCardFrame(forummanager_Forum $forum)
    {
        $this->includeForum();
        return new forummanager_ForumCardFrame($this->App(), $forum);
    }


    /**
     * @param forummanager_Forum $forum
     * @return forummanager_ForumFullFrame
     */
    public function ForumFullFrame(forummanager_Forum $forum)
    {
        $this->includeForum();
        return new forummanager_ForumFullFrame($this->App(), $forum);
    }


    /**
     * Forum editor
     * @return forummanager_ForumEditor
     */
    public function ForumEditor(forummanager_Forum $forum = null)
    {
        $this->includeForum();
        return new forummanager_ForumEditor($this->App(), $forum);
    }





    /**
     * Includes Thread Ui helper functions definitions.
     */
    public function includeThread()
    {
        require_once FUNC_FORUMMANAGER_UI_PATH . 'thread.ui.php';
    }



    /**
     * Thread table view
     * @return forummanager_ThreadTableView
     */
    public function ThreadTableView()
    {
        $this->includeThread();
        return new forummanager_ThreadTableView($this->App());
    }


    /**
     * Thread table view
     * @return forummanager_ThreadCardsView
     */
    public function ThreadCardsView()
    {
        $this->includeThread();
        return new forummanager_ThreadCardsView($this->App());
    }


    /**
     * @param forummanager_Thread $thread
     * @return forummanager_ThreadCardFrame
     */
    public function ThreadCardFrame(forummanager_Thread $thread)
    {
        $this->includeThread();
        return new forummanager_ThreadCardFrame($this->App(), $thread);
    }


    /**
     * @param forummanager_Thread $thread
     * @return forummanager_ThreadFullFrame
     */
    public function ThreadFullFrame(forummanager_Thread $thread)
    {
        $this->includeThread();
        return new forummanager_ThreadFullFrame($this->App(), $thread);
    }


    /**
     * Thread editor
     * @return forummanager_ThreadEditor
     */
    public function ThreadEditor(forummanager_Thread $thread = null)
    {
        $this->includeThread();
        return new forummanager_ThreadEditor($this->App(), $thread);
    }





    /**
     * Includes Post Ui helper functions definitions.
     */
    public function includePost()
    {
        require_once FUNC_FORUMMANAGER_UI_PATH . 'post.ui.php';
    }



    /**
     * Post table view
     * @return forummanager_PostTableView
     */
    public function PostTableView()
    {
        $this->includePost();
        return new forummanager_PostTableView($this->App());
    }


    /**
     * Post table view
     * @return forummanager_PostCardsView
     */
    public function PostCardsView()
    {
        $this->includePost();
        return new forummanager_PostCardsView($this->App());
    }


    /**
     * @param forummanager_Post $post
     * @return forummanager_PostCardFrame
     */
    public function PostCardFrame(forummanager_Post $post)
    {
        $this->includePost();
        return new forummanager_PostCardFrame($this->App(), $post);
    }


    /**
     * @param forummanager_Post $post
     * @return forummanager_PostFullFrame
     */
    public function PostFullFrame(forummanager_Post $post)
    {
        $this->includePost();
        return new forummanager_PostFullFrame($this->App(), $post);
    }


    /**
     * Post editor
     * @return forummanager_PostEditor
     */
    public function PostEditor(forummanager_Post $post = null)
    {
        $this->includePost();
        return new forummanager_PostEditor($this->App(), $post);
    }
}
