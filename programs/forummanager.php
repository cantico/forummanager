<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


$addon_forummanager = bab_getAddonInfosInstance('forummanager');

if (!$addon_forummanager) {
    return;
}

define('FUNC_FORUMMANAGER_PHP_PATH', $addon_forummanager->getPhpPath());
define('FUNC_FORUMMANAGER_SET_PATH', FUNC_FORUMMANAGER_PHP_PATH);
define('FUNC_FORUMMANAGER_UI_PATH', FUNC_FORUMMANAGER_PHP_PATH);
define('FUNC_FORUMMANAGER_PORTLETS_PATH', FUNC_FORUMMANAGER_PHP_PATH . 'portlets/');
bab_functionality::includeFile('App');



/**
 * Func_App_Forummanager
 */
class Func_App_Forummanager extends Func_App
{

    public function __construct()
    {
        parent::__construct();

        $this->addonPrefix = 'forummanager';
        $this->addonName = 'forummanager';
        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/forummanager/main';
    }


    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'Forum manager';
    }




    /**
     * Includes ForumSet class definition.
     */
    public function includeForumSet()
    {
        require_once FUNC_FORUMMANAGER_SET_PATH . 'forum.class.php';
    }

    /**
     * @return string
     */
    public function ForumClassName()
    {
        return $this->classPrefix . 'Forum';
    }

    /**
     * @return string
     */
    public function ForumSetClassName()
    {
        return $this->ForumClassName() . 'Set';
    }


    /**
     * @return forummanager_ForumSet
     */
    public function ForumSet()
    {
        $this->includeForumSet();
        $className = $this->ForumSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes ThreadSet class definition.
     */
    public function includeThreadSet()
    {
        require_once FUNC_FORUMMANAGER_SET_PATH . 'thread.class.php';
    }

    /**
     * @return string
     */
    public function ThreadClassName()
    {
        return $this->classPrefix . 'Thread';
    }

    /**
     * @return string
     */
    public function ThreadSetClassName()
    {
        return $this->ThreadClassName() . 'Set';
    }


    /**
     * @return forummanager_ThreadSet
     */
    public function ThreadSet()
    {
        $this->includeThreadSet();
        $className = $this->ThreadSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes PostSet class definition.
     */
    public function includePostSet()
    {
        require_once FUNC_FORUMMANAGER_SET_PATH . 'post.class.php';
    }

    /**
     * @return string
     */
    public function PostClassName()
    {
        return $this->classPrefix . 'Post';
    }

    /**
     * @return string
     */
    public function PostSetClassName()
    {
        return $this->PostClassName() . 'Set';
    }


    /**
     * @return forummanager_PostSet
     */
    public function PostSet()
    {
        $this->includePostSet();
        $className = $this->PostSetClassName();
        $set = new $className($this);
        return $set;
    }




    function setTranslateLanguage($language)
    {
        parent::setTranslateLanguage($language);
        $translate = bab_functionality::get('Translate/Gettext');
        /* @var $translate Func_Translate_Gettext */
        $translate->setLanguage($language);
    }


    /**
     * Translates the string.
     *
     * @param string $str
     * @return string
     */
    function translate($str, $str_plurals = null, $number = null)
    {
        require_once FUNC_FORUMMANAGER_PHP_PATH . 'functions.php';
        $translation = $str;
        if ($translate = bab_functionality::get('Translate/Gettext')) {
            /* @var $translate Func_Translate_Gettext */
            $translate->setAddonName('forummanager');
            $translation = $translate->translate($str, $str_plurals, $number);
        }
        if ($translation === $str) {
            $translation = parent::translate($str, $str_plurals, $number);
        }

        return $translation;
    }


    /**
     * Includes Controller class definition.
     */
    public function includeController()
    {
        parent::includeController();
        require_once FUNC_FORUMMANAGER_PHP_PATH . 'controller.class.php';
    }

    /**
     *
     * @return forummanager_Controller
     */
    function Controller()
    {
        self::includeController();
        return new forummanager_Controller($this);
    }


    /**
     * Include class forummanager_Access
     */
    protected function includeAccess()
    {
        parent::includeAccess();
        require_once FUNC_FORUMMANAGER_PHP_PATH . 'access.class.php';
    }



    /**
     * Include class forummanager_Ui
     */
    public function includeUi()
    {
        parent::includeUi();
        require_once FUNC_FORUMMANAGER_PHP_PATH . 'ui.class.php';
    }

    /**
     * The forummanager_Ui object propose an access to all ui files and ui objects (widgets)
     *
     * @return forummanager_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance('forummanager_Ui');
    }


    /**
     * Get upload path
     * if the method return null, no upload functionality
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        return new bab_Path(bab_getAddonInfosInstance('forummanager')->getUploadPath());
    }


    /**
     * Include class app_Portlet
     *
     */
    protected function includePortlet()
    {
        parent::includePortlet();
        require_once FUNC_FORUMMANAGER_PORTLETS_PATH . '/portlet.class.php';
    }



    /**
     * The app_Portlet object propose an access to all portlets
     *
     * @return forummanager_Portlet
     */
    public function Portlet()
    {
        $this->includePortlet();
        $portlet = bab_getInstance('forummanager_Portlet')->setApp($this);
        $portlet->includeBase();
        return $portlet;
    }


    /**
     * Returns the configuration for the specified path key
     *
     * @param string    $path           The full path with the key
     * @param mixed     $defaultValue
     * @return mixed
     */
    public function getConf($path, $defaultValue = null)
    {
        static $registry = null;
        if (!isset($registry)) {
            $registry = bab_getRegistry();
        }

        if (substr($path, 0, 1) !== '/') {
            $path = '/' . $path;
        }
        $path = '/' . $this->addonPrefix . $path;

        if (defined($path)) {
            return constant($path);
        }

        $elements = explode('/', $path);
        $key = array_pop($elements);
        $path = implode('/', $elements);
        $registry->changeDirectory($path);
        $value = $registry->getValue($key);

        if (!isset($value)) {
            $value = $defaultValue;
        }

        return $value;
    }
}
