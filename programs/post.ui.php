<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');
$W->includePhpClass('Widget_VBoxLayout');
$W->includePhpClass('Widget_TableModelView');


/**
 * list of posts
 *
 * @method Func_App_Forummanager    App()
 */
class forummanager_PostTableView extends app_TableModelView
{
    /**
     * @param Func_App $app
     * @param string $id
     */
    public function __construct(Func_App $app = null, $id = null)
    {
        parent::__construct($app, $id);
        $this->addClass('depends-forummanager-post');
    }


    /**
     * @param app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * @return app_CtrlRecord
     */
    public function getRecordController()
    {
        return $this->recordController;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $App = $this->App();

        $this->addColumn(
            app_TableModelViewColumn($recordSet->id_thread())
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->subject)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->author)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->date)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->message)
        );

        $this->addColumn(
            app_TableModelViewColumn('_actions_', ' ')
                ->setExportable(false)
                ->setSortable(false)
                ->addClass('widget-column-thin', 'widget-align-center')
        );
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        return parent::handleRow($record, $row);
    }


    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $App = $this->App();

        /* @var $record forummanager_Post */

        switch ($fieldPath) {

            case 'message':
                if ($record->message_format === 'html') {
                    return $W->Html($record->message);
                }
            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Post()->edit($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Post()->confirmDelete($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





class forummanager_PostCardsView extends forummanager_PostTableView
{

    /**
     *
     * @param Func_App $App
     * @param string $id
     */
    public function __construct(Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setSpacing(1, 'em');
        $this->setLayout($layout);
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $card = $Ui->PostCardFrame($record);
        $card->addClass('crm-card');
        $card->setSizePolicy('col-md-12');
        $this->addItem($card);
        return true;
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->init();

        $items = array();

        $total = $this->handleTotalDisplay();
        $selector = $this->handlePageSelector();


        if (null !== $total) {
            $items[] = $total->display($canvas);
        }

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js');
    }
}



class forummanager_CardFrame extends app_UiObject
{
    /**
     * @param Func_App $App
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct(Func_App $App, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($App);

        $W = bab_Widgets();

        $this->setInheritedItem($W->Frame($id, $layout));
    }


    /**
     * @param string $labelText
     * @param Widget_Displayable_Interface $item
     * @param string $fieldName
     * @param string $description
     * @param string $suffix
     * @param string $balloon
     *
     * @return Widget_LabelledWidget
     */
    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null, $balloon = null)
    {
        $W = bab_Widgets();

        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix, $balloon);
    }
}


class forummanager_PostFrame extends forummanager_CardFrame
{
    /**
     * @var forummanager_Post
     */
    protected $post;

    /**
     * @var forummanager_PostSet
     */
    protected $set;


    /**
     * @param Func_App $App
     * @param forummanager_Post $post
     * @param string $id
     */
    public function __construct(Func_App $App, forummanager_Post $post, $id = null, $layout = null)
    {
        parent::__construct($App, $id, $layout);

        $this->post = $post;
        $this->set = $post->getParentSet();
    }



    protected function author()
    {
        return null;

        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Author'),
            bab_getUserName($this->author, true)
        );
    }




    protected function message()
    {
        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Message'),
            $this->message
        );
    }
}


class forummanager_PostCardFrame extends forummanager_CardFrame
{

    /**
     * @var int
     */
    protected $attributes;

    protected $section = null;
    protected $inlineMenu = null;
    protected $popupMenu = null;
    protected $toolbar = null;

    static $now = null;


    /**
     * @param Func_App $App
     * @param forummanager_Post $post
     * @param int $attributes
     * @param string $id
     */
    public function __construct(Func_App $App, forummanager_Post $post, $id = null)
    {
        $W = bab_Widgets();

        $box = $W->FlowItems();
        $box->setVerticalAlign('top');

        parent::__construct($App, $id, $box);

        $postSet = $post->getParentSet();

        $box->addItem(
            $W->VBoxItems(
                $W->Label(bab_getUserName($post->id_author, true))
            )->setSizePolicy('widget-30pc')
        );
        $box->addItem(
            $W->VBoxItems(
                $W->Label($postSet->date->output($post->date))
                    ->addClass('widget-small'),
                $W->Html($post->message)
            )->setSizePolicy('widget-70pc')
            ->setVerticalSpacing(1, 'em')
        );
    }



    /**
     * @param Widget_Displayable_Item $item
     *
     * @return self
     */
    public function addLinkedItem($item)
    {
        $this->inlineMenu->addItem($item);

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     * @param bool $important
     *
     * @return self
     */
    public function addMenuItem(Widget_Displayable_Interface $item, $classname = null, $important = true)
    {
        if (!isset($this->popupMenu)) {
            $this->popupMenu = $this->section->addContextMenu('popup');
            $this->popupMenu->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);
        }
        $this->popupMenu->addItem($item);

        $item->addClass('icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     *
     * @return forummanager_PostCardFrame
     */
    public function addToolbarItem(Widget_Displayable_Interface $item, $classname = null)
    {
        $W = bab_Widgets();
        if (!isset($this->toolbar)) {
            $this->toolbar = $W->FlowLayout()
            ->setSpacing(4, 'px')
            ->addClass('icon-left-16 icon-left icon-16x16 crm-small');
            $this->section->addItem($this->toolbar);
        }
        $this->toolbar->addItem($item->addClass('widget-actionbutton'));

        $item->addClass('widget-icon icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }
        return $this;
    }

}


class forummanager_PostFullFrame extends forummanager_PostCardFrame
{
    /**
     * @param Func_App $App
     * @param forummanager_Post $post
     * @param unknown $id
     */
    public function __construct(Func_App $App, forummanager_Post $post, $id = null)
    {
        $W = bab_Widgets();
        parent::__construct($App, $post, $id, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->post = $post;
        $this->set = $post->getParentSet();

        $this->addInfos();

        $this->addClass('crm-detailed-info-frame');
        $this->addClass(Func_Icons::ICON_LEFT_24);
    }


    protected function addInfos()
    {
        $App = $this->App();
        $W = bab_Widgets();
        $this->addItem($this->getName());

        if ($this->post->isCompleted()) {
            $this->addItem($W->Title($App->translate('Completed post'), 3));
        }

        $this->addItem($this->responsible());

        $this->addItem($this->createdBy());


        $this->addItem(
            $W->FlowItems(
                $this->dueDate(),
                $this->scheduledStart(),
                $this->scheduledFinish()
            )->setHorizontalSpacing(4, 'em')
        );

        $this->addItem($this->description());

        $this->addItem($this->durationQuantities());

        $this->addItem($this->completion());

        $this->addItem($this->startedAction());
    }


    protected function dueDate()
    {
        return $this->LabelledWidget(
            forummanager_translate('Due date'),
            $this->post,
            $this->set->dueDate
        );
    }



    protected function scheduledDate($label, $name)
    {
        $value = $this->post->$name;
        $field = $this->set->$name;

        if (!$field->isValueSet($value)) {
            return null;
        }

        $displayable = $field->output($value);

        if ($this->post->scheduledAllDay) {
            list($displayable) = explode(' ', $displayable);
        }

        return $this->LabelledWidget($label, $displayable);
    }



    protected function scheduledStart()
    {
        return $this->scheduledDate(
            forummanager_translate('Scheduled start date'),
            'scheduledStart'
        );
    }


    protected function scheduledFinish()
    {
        return $this->scheduledDate(
            forummanager_translate('Scheduled finish date'),
            'scheduledFinish'
        );
    }



    /**
     * @return Widget_Layout
     */
    protected function durationQuantities()
    {
        $W = bab_Widgets();

        return $W->FlowItems(
            $this->work(),
            $this->actualWork(),
            $this->remainingWork()
        )->setHorizontalSpacing(3, 'em');
    }
}




/**
 * @return app_Editor
 */
class forummanager_PostEditor extends app_Editor
{
    /**
     * Add fields into form
     * @return crm_NoteEditor
     */
    public function prependFields()
    {
        return $this;
    }

    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();

        $this->addItem($this->subject());
        $this->addItem($this->message());

        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
        }

        return $this;
    }


    protected function subject()
    {
        $App = $this->App();

        $postSet = $App->PostSet();

        return $this->labelledField(
            $App->translate('Subject'),
            app_OrmWidget($postSet->subject),
            'subject'
        );
    }

    protected function message()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $postSet = $App->PostSet();

        return $this->labelledField(
            $App->translate('message'),
            $W->BabHtmlEdit()
                ->setMandatory(true, $App->translate('The post message must not be empty.')),
            'message'
        );
    }
}

