<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2017 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

$App = forummanager_App();

$App->includeController();



/**
 * This controller manages actions that can be performed on forums.
 *
 * @method Func_App_Forummanager App()
 */
class forummanager_CtrlThread extends forummanager_Controller
{

    /**
     * {@inheritDoc}
     * @see app_CtrlRecord::toolbar()
     */
    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $toolbar = parent::toolbar($tableView); //$Ui->toolbar();

        $toolbar->addItem(
            $W->ActionButton(
                '',
                $this->proxy()->edit()
            )->addClass('icon',  Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG),
            0
        );

        return $toolbar;
    }




    /**
     * Actions frame for thread display
     * @param forummanager_Thread $thread
     * @return Widget_VBoxLayout
     */
    protected function getActionsFrame(forummanager_Thread $thread)
    {
        $App = $this->App();
        $Access = $App->Access();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        $actionsFrame = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'px')
            ->addClass(Func_Icons::ICON_LEFT_16)
            ->addClass('crm-actions');

        $Ctrl = $App->Controller()->Thread();


        if ($Access->updateThread($thread->id)) {
            $actionsFrame->addItem($W->Link($W->Icon($App->translate('Edit this thread'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Ctrl->edit($thread->id)));
        }

        if ($Access->deleteThread($thread->id)) {
            $actionsFrame->addItem(
                $W->Link($W->Icon($App->translate('Delete this thread'), Func_Icons::ACTIONS_EDIT_DELETE), $Ctrl->confirmDelete($thread->id))->setOpenMode(Widget_Link::OPEN_DIALOG)
            );
        }

        return $actionsFrame;
    }


    public function displayForumList($forum = null, $filter = null, $type = null)
    {
        $filter['forum'] = $forum;
        return parent::displayList($filter, $type);
    }
}
