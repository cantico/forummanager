<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');
$W->includePhpClass('Widget_VBoxLayout');
$W->includePhpClass('Widget_TableModelView');


/**
 * list of forums
 *
 * @method Func_App_Forummanager    App()
 */
class forummanager_ForumTableView extends app_TableModelView
{
    private $threadSet = null;

    private $postSet = null;

    /**
     * @param Func_App $app
     * @param string $id
     */
    public function __construct(Func_App $app = null, $id = null)
    {
        parent::__construct($app, $id);
        $this->addClass('depends-forummanager-forum');
    }


    /**
     * @return forummanager_ThreadSet
     */
    private function threadSet()
    {
        if (!isset($this->threadSet)) {
            $App = $this->App();
            $this->threadSet = $App->ThreadSet();
        }

        return $this->threadSet;
    }

    /**
     * @return forummanager_PostSet
     */
    private function postSet()
    {
        if (!isset($this->postSet)) {
            $App = $this->App();
            $this->postSet = $App->PostSet();
        }

        return $this->postSet;
    }

    /**
     * @param app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * @return app_CtrlRecord
     */
    public function getRecordController()
    {
        return $this->recordController;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $App = $this->App();
        $this->addColumn(
            app_TableModelViewColumn($recordSet->name)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->description)
        );
        $this->addColumn(
            app_TableModelViewColumn('threads', $App->translate('Threads'))
                ->addClass('widget-align-center')
        );
        $this->addColumn(
            app_TableModelViewColumn('posts', $App->translate('Posts'))
                ->addClass('widget-align-center')
        );

        $this->addColumn(
            app_TableModelViewColumn('_actions_', ' ')
                ->setExportable(false)
                ->setSortable(false)
                ->addClass('widget-column-thin', 'widget-align-center')
        );
    }



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        return parent::handleRow($record, $row);
    }


    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $App = $this->App();

        /* @var $record forummanager_Forum */

        switch ($fieldPath) {
            case 'name':
                return $W->Link(
                    $record->name,
                    $App->Controller()->Thread()->displayForumList($record->id)
                );

            case 'description':
                return $W->Html(nl2br($record->description));

            case 'threads':
                $threadSet = $this->threadSet();
                $threads = $threadSet->select($threadSet->forum->is($record->id));
                return $W->Link(
                    $threads->count(),
                    $App->Controller()->Thread()->displayForumList($record->id)
                );

            case 'posts':
                $postSet = $this->postSet();
                $posts = $postSet->select($postSet->id_thread()->forum->is($record->id));
                return $W->Label($posts->count());

            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Forum()->edit($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Forum()->confirmDelete($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





class forummanager_ForumCardsView extends forummanager_ForumTableView
{

    /**
     *
     * @param Func_App $App
     * @param string $id
     */
    public function __construct(Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setSpacing(1, 'em');
        $this->setLayout($layout);
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $card = $Ui->ForumCardFrame($record);
        $card->addClass('crm-card');
        $card->setSizePolicy('col-xs-12 col-sm-6 col-md-4 col-lg-3');
        $this->addItem($card);
        return true;
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->init();

        $items = array();

        $total = $this->handleTotalDisplay();
        $selector = $this->handlePageSelector();


        if (null !== $total) {
            $items[] = $total->display($canvas);
        }

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js');
    }
}



class forummanager_CardFrame extends app_UiObject
{
    /**
     * @param Func_Crm $crm
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct(Func_App $App, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($App);

        $W = bab_Widgets();

        $this->setInheritedItem($W->Frame($id, $layout));
    }


    /**
     * @param string $labelText
     * @param Widget_Displayable_Interface $item
     * @param string $fieldName
     * @param string $description
     * @param string $suffix
     * @param string $balloon
     *
     * @return Widget_LabelledWidget
     */
    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null, $balloon = null)
    {
        $W = bab_Widgets();

        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix, $balloon);
    }
}


class forummanager_ForumFrame extends forummanager_CardFrame
{
    /**
     * @var forummanager_Forum
     */
    protected $forum;

    /**
     * @var forummanager_ForumSet
     */
    protected $set;


    /**
     * @param Func_App $App
     * @param forummanager_Forum $forum
     * @param string $id
     * @param unknown $layout
     */
    public function __construct(Func_App $App, forummanager_Forum $forum, $id = null, $layout = null)
    {
        parent::__construct($App, $id, $layout);

        $this->forum = $forum;
        $this->set = $forum->getParentSet();
    }



    protected function responsible()
    {
        if (!$this->forum->responsible) {
            return null;
        }

        return null;

        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Responsible'),
            $this->forum,
            $this->set->responsible
        );
    }


    protected function getName()
    {
        $W = bab_Widgets();
        return $W->Title(implode(' / ', $this->forum->getPathName()), 1);
    }

    protected function createdBy()
    {
        if (!$this->set->createdBy->isValueSet($this->forum->createdBy)) {
            return null;
        }

        if ($this->forum->createdBy === $this->forum->responsible) {
            return null;
        }

        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Created by'),
            $this->set->createdBy->output($this->forum->createdBy)
        );
    }




    protected function description()
    {
        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Description'),
            $this->forum,
            $this->set->description
        );
    }


    protected function completion()
    {
        $W = bab_Widgets();

        $completion = $this->forum->getTreeCompletion();

        if (!$completion) {
            return null;
        }

        return $W->Gauge()->setProgress($completion);
    }
}


class forummanager_ForumFullFrame extends forummanager_ForumCardFrame
{
    /**
     * @param Func_App $App
     * @param forummanager_Forum $forum
     * @param unknown $id
     */
    public function __construct(Func_App $App, forummanager_Forum $forum, $id = null)
    {
        $W = bab_Widgets();
        parent::__construct($App, $forum, $id, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->forum = $forum;
        $this->set = $forum->getParentSet();

        $this->addInfos();

        $this->addClass('crm-detailed-info-frame');
        $this->addClass(Func_Icons::ICON_LEFT_24);
    }


    protected function addInfos()
    {
        $App = $this->App();
        $W = bab_Widgets();
        $this->addItem($this->getName());

        if ($this->forum->isCompleted()) {
            $this->addItem($W->Title($App->translate('Completed forum'), 3));
        }

        $this->addItem($this->responsible());

        $this->addItem($this->createdBy());


        $this->addItem(
            $W->FlowItems(
                $this->dueDate(),
                $this->scheduledStart(),
                $this->scheduledFinish()
            )->setHorizontalSpacing(4, 'em')
        );

        $this->addItem($this->description());

        $this->addItem($this->durationQuantities());

        $this->addItem($this->completion());

        $this->addItem($this->startedAction());
    }


    protected function dueDate()
    {
        return $this->LabelledWidget(
            forummanager_translate('Due date'),
            $this->forum,
            $this->set->dueDate
        );
    }



    protected function scheduledDate($label, $name)
    {
        $value = $this->forum->$name;
        $field = $this->set->$name;

        if (!$field->isValueSet($value)) {
            return null;
        }

        $displayable = $field->output($value);

        if ($this->forum->scheduledAllDay) {
            list($displayable) = explode(' ', $displayable);
        }

        return $this->LabelledWidget($label, $displayable);
    }



    protected function scheduledStart()
    {
        return $this->scheduledDate(
            forummanager_translate('Scheduled start date'),
            'scheduledStart'
        );
    }


    protected function scheduledFinish()
    {
        return $this->scheduledDate(
            forummanager_translate('Scheduled finish date'),
            'scheduledFinish'
        );
    }



    /**
     * @return Widget_Layout
     */
    protected function durationQuantities()
    {
        $W = bab_Widgets();

        return $W->FlowItems(
            $this->work(),
            $this->actualWork(),
            $this->remainingWork()
        )->setHorizontalSpacing(3, 'em');
    }
}




class forummanager_ForumCardFrame extends forummanager_ForumFrame
{

    const SHOW_ALL = 0xFFFF;


    /**
     * @var forummanager_Forum
     */
    protected $forum;

    /**
     * @var int
     */
    protected $attributes;

    protected $section = null;
    protected $inlineMenu = null;
    protected $popupMenu = null;
    protected $toolbar = null;

    static $now = null;


    /**
     * @param Func_App $App
     * @param forummanager_Forum $forum
     * @param int $attributes
     * @param string $id
     */
    public function __construct(Func_App $App, forummanager_Forum $forum, $attributes = forummanager_ForumCardFrame::SHOW_ALL, $id = null)
    {
        $W = bab_Widgets();

        parent::__construct($App, $forum, $id, $W->Layout());

        $Access = $App->Access();

        if (!isset(self::$now)) {
            self::$now = BAB_DateTime::now();
            self::$now = self::$now->getIsoDate();
        }


        $this->forum = $forum;
        $this->attributes = $attributes;

        $forumSection = $W->Section(
            $forum->getNameWidget(),
            $this->forumContent = $W->VBoxItems(
                $W->RichText($forum->description)
            )->setVerticalSpacing(.5,'em')->addClass('crm-small'),
            6
        );
        $forumSection->setFoldable(true, true);
        if ($forum->dueDate !== '0000-00-00') {
            $dateDiff = BAB_DateTime::dateDiffIso(self::$now, $forum->dueDate);
            if ($dateDiff > 1)
            {
                $days = $App->translate('days');
            } else {
                $days = $App->translate('day');
            }

            if (self::$now <  $forum->dueDate) {
                $forumSection->setSubHeaderText(BAB_DateTimeUtil::relativeFutureDate($forum->dueDate, false, false) . ' (' . $dateDiff . ' '.$days.')');
            } else {
                $forumSection->setSubHeaderText(BAB_DateTimeUtil::relativePastDate($forum->dueDate, false, false) . ' (' . $dateDiff .' '.$days.')');
            }
        }
        $forumSection->addClass('crm-forum');

        $this->section = $forumSection;
        $this->inlineMenu = $this->section->addContextMenu('inline');

        $forumSection->addClass(Func_Icons::ICON_LEFT_16);

        $this->addItem(
            $forumSection
        );

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $this->item->setCacheHeaders(Widget_Item::CacheHeaders()->setLastModified(BAB_DateTime::fromIsoDateTime($forum->modifiedOn)));


        // add owner if not me

        if ($forum->responsible > 0 && bab_getUserId() != $forum->responsible) {
            $forumSection->addItem($this->responsible());
        }

        $forumSection->addItem($this->remainingWork());
        $forumSection->addItem($this->completion());

        $this->addMenuItem(
            $W->Link(
                $App->translate('View details'),
                $App->Controller()->Forum()->display($forum->id)
            ),
            Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        );
        $this->addToolbarItem(
            $W->Link(
                $App->translate('View details'),
                $App->Controller()->Forum()->display($forum->id)
            ),
            Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        );
    }



    /**
    * @param Widget_Displayable_Item $item
    *
    * @return self
    */
    public function addLinkedItem($item)
    {
        $this->inlineMenu->addItem($item);

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     * @param bool $important
     *
     * @return self
     */
    public function addMenuItem(Widget_Displayable_Interface $item, $classname = null, $important = true)
    {
        if (!isset($this->popupMenu)) {
            $this->popupMenu = $this->section->addContextMenu('popup');
            $this->popupMenu->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);
        }
        $this->popupMenu->addItem($item);

        $item->addClass('icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     *
     * @return forummanager_ForumCardFrame
     */
    public function addToolbarItem(Widget_Displayable_Interface $item, $classname = null)
    {
        $W = bab_Widgets();
        if (!isset($this->toolbar)) {
            $this->toolbar = $W->FlowLayout()
                ->setSpacing(4, 'px')
                ->addClass('icon-left-16 icon-left icon-16x16 crm-small');
            $this->section->addItem($this->toolbar);
        }
        $this->toolbar->addItem($item->addClass('widget-actionbutton'));

        $item->addClass('widget-icon icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }
        return $this;
    }

}





/**
 * @return app_Editor
 */
class forummanager_ForumEditor extends app_Editor
{
    /**
     * Add fields into form
     * @return crm_NoteEditor
     */
    public function prependFields()
    {
        return $this;
    }

    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();

        $this->addItem($this->name());
        $this->addItem($this->description());

        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
        }

        return $this;
    }



    protected function name()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Name'),
            app_OrmWidget($this->getRecordSet()->name),
            'name'
        );
    }

    protected function description()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Description'),
            app_OrmWidget($this->getRecordSet()->description),
            'description'
        );
    }
}
