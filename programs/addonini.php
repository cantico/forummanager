;<?php/*

[general]
name="forummanager"
version="0.9.80"
addon_type="EXTENSION"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description=""
description.fr=""
delete=1
ov_version="8.6.97"
php_version="5.4.0"
addon_access_control="1"
author="Laurent Choulette (laurent.choulette@cantico.fr)"
icon="forummanager48.png"

[addons]
libapp="0.0.1"
liborm="0.11.12"
widgets="1.1.0"
portlets="0.23.0"

[functionalities]

; */?>
