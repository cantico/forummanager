<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = forummanager_App();

$App->includeTraceableRecordSet();


/**
 * @param ORM_PkField                   $id
 * @param ORM_DatetimeField             $date
 * @param ORM_DatetimeField             $dateupdate
 * @param ORM_DatetimeField             $date_confirm
 * @param ORM_TextField                 $author
 * @param ORM_IntField                  $id_author
 * @param ORM_StringField               $subject
 * @param ORM_TextField                 $message
 * @param ORM_StringField               $message_format
 * @param ORM_EnumField                 $confirmed
 *
 * @param forummanager_ThreadSet        $id_thread
 * @param forummanager_PostSet          $id_parent
 *
 * @method forummanager_Post get()
 * @method forummanager_Post request()
 * @method forummanager_Post newRecord()
 * @method forummanager_Post[] select()
 */
class forummanager_PostSet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName(BAB_POSTS_TBL);
        $App = $this->App();

        $this->setDescription($App->translatable('Post'));

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_DatetimeField('date')
                ->setDescription($App->translatable('Date')),
            ORM_DatetimeField('dateupdate')
                ->setDescription($App->translatable('Update date')),
            ORM_DatetimeField('date_confirm')
                ->setDescription($App->translatable('Confirmation date')),
            ORM_TextField('author')
                ->setDescription($App->translatable('Author name')),
            ORM_UserField('id_author')
                ->setDescription($App->translatable('Author')),
            ORM_StringField('subject', 100)
                ->setDescription($App->translatable('Subject')),
            ORM_TextField('message')
                ->setDescription($App->translatable('Message')),
            ORM_StringField('message_format', 32)
                ->setDescription($App->translatable('Message format')),
            ORM_EnumField('confirmed', array('N' => 'N', 'Y' => 'Y'))
        );

        $this->hasOne('id_thread', $App->ThreadSetClassName())
            ->setDescription($App->translatable('Thread'));

        $this->hasOne('id_parent', $App->PostClassName())
            ->setDescription($App->translatable('In reply to'));
    }


    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @param int                   $id
 * @param string                $date
 * @param string                $dateupdate
 * @param string                $date_confirm
 * @param string                $author
 * @param int                   $id_author
 * @param string                $subject
 * @param string                $message
 * @param string                $message_format
 * @param string                $confirmed
 *
 * @param forummanager_Thread   $id_thread
 * @param forummanager_Post     $id_parent
 */
class forummanager_Post extends app_Record
{
}
