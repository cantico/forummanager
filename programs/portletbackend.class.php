<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

bab_Functionality::includefile('PortletBackend');



$GLOBALS['Func_PortletBackend_Forummanager_Categories'] = array(
    'portal_tools' => forummanager_translate('Portal tools')
);


/**
 * Crm Portlet backend
 */
class Func_PortletBackend_Forummanager extends Func_PortletBackend
{

    /**
     * @return string
     */
    public function getDescription()
    {
        return forummanager_translate('Forummanager');
    }


    /**
     * @param string $category
     * @return forummanager_PortletDefinition_Forummanager[]
     */
    public function select($category = null)
    {
        $addon = bab_getAddonInfosInstance('forummanager');
        if (!$addon || !$addon->isAccessValid()) {
            return array();
        }

        global $Func_PortletBackend_Forummanager_Categories;
        if (empty($category) || in_array($category, $Func_PortletBackend_Forummanager_Categories)) {
            return array(
                'Forummanager' => $this->portlet_Forummanager(),
            );
        }

        return array();
    }


    /**
     * @return forummanager_PortletDefinition_Forummanager
     */
    public function portlet_Forummanager()
    {
        return new forummanager_PortletDefinition_Forummanager();
    }


    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        global $Func_PortletBackend_Forummanager_Categories;

        return $Func_PortletBackend_Forummanager_Categories;
    }

    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}







////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////


class forummanager_PortletDefinition_Forummanager implements portlet_PortletDefinitionInterface
{
    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;

    /**
     *
     */
    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('forummanager');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getId()
     */
    public function getId()
    {
        return 'Forummanager';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getName()
     */
    public function getName()
    {
        return forummanager_translate('Forummanager');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getDescription()
     */
    public function getDescription()
    {
        return forummanager_translate('Configurable forum view.');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getPortlet()
     */
    public function getPortlet()
    {
        return new forummanager_Portlet_Forummanager();
    }


    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        return array(
            array(
                'type' => 'list',
                'name' => 'displayType',
                'label' => forummanager_translate('Display type'),
                'options' => array(
                    array(
                        'label' => forummanager_translate('List'),
                        'value' => 'list'
                    ),
                )
            ),
        );
    }


    /**
     *
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getRichIcon()
     */
    public function getRichIcon()
    {
        return $this->addon->getImagesPath() . 'forummanager128.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getIcon()
     */
    public function getIcon()
    {
        return $this->addon->getImagesPath() . 'forummanager48.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getThumbnail()
     */
    public function getThumbnail()
    {
        return $this->addon->getImagesPath() . 'thumbnail.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}



class forummanager_Portlet_Forummanager extends Widget_Item implements portlet_PortletInterface
{

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * {@inheritDoc}
     * @see Widget_Widget::getName()
     */
    public function getName()
    {
        return get_class($this);
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::getPortletDefinition()
     */
    public function getPortletDefinition()
    {
        return new forummanager_PortletDefinition_Forummanager();
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreferences()
     */
    public function setPreferences(array $configuration)
    {
        bab_Registry::override('/forummanager/' . $this->id . '/displayType', $configuration['displayType']);
    }

    /**
     * {@inheritDoc}
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $App = forummanager_App();
        $addon = bab_getAddonInfosInstance('forummanager');

        $ctrl = $App->Controller()->Forum(false);



        $box = $ctrl->filteredView(null, null, $this->id);
//        $box->setId('forummanager_' . $this->id); // The widget item id.

        $display = $box->display($canvas);

        return $display;
    }



    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreference()
     */
    public function setPreference($name, $value)
    {

    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPortletId()
     */
    public function setPortletId($id)
    {
        $this->id = $id;
    }
}
