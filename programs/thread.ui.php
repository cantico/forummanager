<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');
$W->includePhpClass('Widget_VBoxLayout');
$W->includePhpClass('Widget_TableModelView');


/**
 * list of threads
 *
 * @method Func_App_Forummanager    App()
 */
class forummanager_ThreadTableView extends app_TableModelView
{

    private $postSet = null;

    /**
     * @return forummanager_PostSet
     */
    private function postSet()
    {
        if (!isset($this->postSet)) {
            $App = $this->App();
            $this->postSet = $App->PostSet();
        }

        return $this->postSet;
    }

    /**
     * @param Func_App $app
     * @param string $id
     */
    public function __construct(Func_App $app = null, $id = null)
    {
        parent::__construct($app, $id);
        $this->addClass('depends-forummanager-thread');
    }


    /**
     * @param app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * @return app_CtrlRecord
     */
    public function getRecordController()
    {
        return $this->recordController;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $App = $this->App();

        $this->addColumn(
            app_TableModelViewColumn($recordSet->forum(), $App->translate('Forum'))
                ->setVisible(false)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->post()->subject)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->post()->author)
        );
        $this->addColumn(
            app_TableModelViewColumn('posts', $App->translate('Posts'))
                ->addClass('widget-align-center')
        );
        $this->addColumn(
            app_TableModelViewColumn('_actions_', ' ')
                ->setExportable(false)
                ->setSortable(false)
                ->addClass('widget-column-thin', 'widget-align-center')
        );
    }



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        return parent::handleRow($record, $row);
    }


    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $App = $this->App();

        /* @var $record forummanager_Thread */

        switch ($fieldPath) {

            case 'post/subject':
                return $W->Link(
                $record->post->subject,
                $App->Controller()->Post()->displayThreadList($record->id)
            );


            case 'posts':
                $postSet = $this->postSet();
                $posts = $postSet->select($postSet->id_thread->is($record->id));
                return $W->Link(
                    $posts->count(),
                    $App->Controller()->Post()->displayList()
                );

            case '_actions_':
                $box = $W->HBoxItems();
                $box->setSizePolicy(Func_Icons::ICON_LEFT_16);
//                 if ($record->isUpdatable()) {
//                     $box->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
//                     $box->addItem(
//                         $W->Link(
//                             '', $this->App()->Controller()->Thread()->edit($record->id)
//                         )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
//                         ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
//                     );
//                 }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Thread()->confirmDelete($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





class forummanager_ThreadCardsView extends forummanager_ThreadTableView
{

    /**
     *
     * @param Func_App $App
     * @param string $id
     */
    public function __construct(Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setSpacing(1, 'em');
        $this->setLayout($layout);
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $card = $Ui->ThreadCardFrame($record);
        $card->addClass('crm-card');
        $card->setSizePolicy('col-xs-12 col-sm-6 col-md-4 col-lg-3');
        $this->addItem($card);
        return true;
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->init();

        $items = array();

        $total = $this->handleTotalDisplay();
        $selector = $this->handlePageSelector();


        if (null !== $total) {
            $items[] = $total->display($canvas);
        }

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js');
    }
}



class forummanager_CardFrame extends app_UiObject
{
    /**
     * @param Func_Crm $crm
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct(Func_App $App, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($App);

        $W = bab_Widgets();

        $this->setInheritedItem($W->Frame($id, $layout));
    }


    /**
     * @param string $labelText
     * @param Widget_Displayable_Interface $item
     * @param string $fieldName
     * @param string $description
     * @param string $suffix
     * @param string $balloon
     *
     * @return Widget_LabelledWidget
     */
    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null, $balloon = null)
    {
        $W = bab_Widgets();

        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix, $balloon);
    }
}


class forummanager_ThreadFrame extends forummanager_CardFrame
{
    /**
     * @var forummanager_Thread
     */
    protected $thread;

    /**
     * @var forummanager_ThreadSet
     */
    protected $set;


    /**
     * @param Func_App $App
     * @param forummanager_Thread $thread
     * @param string $id
     * @param unknown $layout
     */
    public function __construct(Func_App $App, forummanager_Thread $thread, $id = null, $layout = null)
    {
        parent::__construct($App, $id, $layout);

        $this->thread = $thread;
        $this->set = $thread->getParentSet();
    }



    protected function responsible()
    {
        if (!$this->thread->responsible) {
            return null;
        }

        return null;

        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Responsible'),
            $this->thread,
            $this->set->responsible
        );
    }


    protected function getName()
    {
        $W = bab_Widgets();
        return $W->Title(implode(' / ', $this->thread->getPathName()), 1);
    }

    protected function createdBy()
    {
        if (!$this->set->createdBy->isValueSet($this->thread->createdBy)) {
            return null;
        }

        if ($this->thread->createdBy === $this->thread->responsible) {
            return null;
        }

        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Created by'),
            $this->set->createdBy->output($this->thread->createdBy)
        );
    }




    protected function description()
    {
        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Description'),
            $this->thread,
            $this->set->description
        );
    }
}


class forummanager_ThreadFullFrame extends forummanager_ThreadCardFrame
{

}




class forummanager_ThreadCardFrame extends forummanager_ThreadFrame
{

}





/**
 * @return app_Editor
 */
class forummanager_ThreadEditor extends app_Editor
{
    /**
     * Add fields into form
     * @return crm_NoteEditor
     */
    public function prependFields()
    {
        return $this;
    }

    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();

        $this->addItem($this->subject());
        $this->addItem($this->summary());
        $this->addItem($this->dueDate());
        $this->addItem($this->description());
        $this->addItem($this->responsible());

        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
        }

        return $this;
    }


    protected function subject()
    {
        $W = bab_Widgets();
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Summary'),
            $W->LineEdit()
                ->setMandatory(true, $App->translate('The thread summary must not be empty.')),
            'summary'
        );
    }

    protected function dueDate()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Due date'),
            app_OrmWidget($this->getRecordSet()->dueDate),
            'dueDate'
        );
    }

    protected function description()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Description'),
            app_OrmWidget($this->getRecordSet()->description),
            'description'
        );
    }

    protected function responsible()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Responsible'),
            app_OrmWidget($this->getRecordSet()->responsible),
            'responsible'
        );
    }
}

