<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = forummanager_App();

$App->includeTraceableRecordSet();


/**
 * @param ORM_PkField                   $id
 * @param ORM_StringField               $name
 * @param ORM_TextField                 $description
 * @param ORM_EnumField                 $moderation
 * @param ORM_EnumField                 $notification
 * @param ORM_IntField                  $display
 * @param ORM_EnumField                 $active
 * @param ORM_IntField                  $ordering
 * @param ORM_IntField                  $id_dgowner
 * @param ORM_EnumField                 $bdisplayemailaddress
 * @param ORM_EnumField                 $bdisplayauhtordetails
 * @param ORM_EnumField                 $bflatview
 * @param ORM_EnumField                 $bupdatemoderator
 * @param ORM_EnumField                 $bupdateauthor
 *
 * @method forummanager_Forum get()
 * @method forummanager_Forum request()
 * @method forummanager_Forum newRecord()
 * @method forummanager_Forum[] select()
 */
class forummanager_ForumSet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName(BAB_FORUMS_TBL);
        $App = $this->App();

        $this->setDescription($App->translatable('Forum'));

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription($App->translatable('Name')),
            ORM_TextField('description')
                ->setDescription($App->translatable('Description')),
            ORM_EnumField('moderation', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('notification', array('N' => 'N', 'Y' => 'Y')),
            ORM_IntField('display'),
            ORM_EnumField('active', array('N' => 'N', 'Y' => 'Y')),
            ORM_IntField('ordering'),
            ORM_IntField('id_dgowner'),
            ORM_EnumField('bdisplayemailaddress', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('bdisplayauhtordetails', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('bflatview', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('bupdatemoderator', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('bupdateauthor', array('N' => 'N', 'Y' => 'Y'))
        );
    }

    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @param int               $id
 * @param string            $name
 * @param string            $description
 * @param string            $moderation
 * @param string            $notification
 * @param int               $display
 * @param string            $active
 * @param int               $ordering
 * @param int               $id_dgowner
 * @param string            $bdisplayemailaddress
 * @param string            $bdisplayauhtordetails
 * @param string            $bflatview
 * @param string            $bupdatemoderator
 * @param string            $bupdateauthor
 */
class forummanager_Forum extends app_Record
{
}
